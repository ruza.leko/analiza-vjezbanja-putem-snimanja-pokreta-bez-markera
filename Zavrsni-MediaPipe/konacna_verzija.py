import cv2
import mediapipe as mp
import numpy as np
import math
import csv
import os
import pickle
import pandas as pd

grade_colors = {          # Dictionary
    "Good": (0, 215, 0),  # Green for good grade
    "Bad": (0, 0, 255)    # Red for bad grade
}#<<<<<<<<<<<<<<<<<<<

mp_drawing = mp.solutions.drawing_utils
mp_pose = mp.solutions.pose

# Load the pose detection model
with open('pose_det.pkl', 'rb') as f:
    model = pickle.load(f)

# Initialize video capture
cap = cv2.VideoCapture(0)
screen_width = 1920  # Replace with your screen's width
screen_height = 1080  # Replace with your screen's height
cv2.namedWindow('Mediapipe Feed', cv2.WINDOW_NORMAL)
cv2.setWindowProperty('Mediapipe Feed', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)#<<<<<<<<<<<<<<

# Setup mediapipe instance
with mp_pose.Pose(min_detection_confidence=0.5, min_tracking_confidence=0.5) as pose_model:
    while cap.isOpened():
        ret, frame = cap.read()

        # Recolor image to RGB
        image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        image.flags.writeable = False

        # Make detection
        results = pose_model.process(image)

        # Recolor back to BGR
        image.flags.writeable = True
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

        # Render detections
        mp_drawing.draw_landmarks(image, results.pose_landmarks, mp_pose.POSE_CONNECTIONS,
                                  mp_drawing.DrawingSpec(color=(245, 117, 66), thickness=2, circle_radius=2),
                                  mp_drawing.DrawingSpec(color=(255, 255, 255), thickness=2, circle_radius=2))

        # Export coordinates
        try:
            # Extract Pose landmarks
            pose = results.pose_landmarks.landmark
            pose_row = list(np.array([[landmark.x, landmark.y, landmark.z, landmark.visibility] for landmark in pose]).flatten())

            # Make Detections
            X = pd.DataFrame([pose_row])
            body_language_class = model.predict(X)[0]
            body_language_prob = model.predict_proba(X)[0]

            # Get status box
            cv2.rectangle(image, (0, 0), (250, 60), (255, 255, 255),)

            def calculate_angle(a, b, c):
                            angle_rad = math.atan2(c[1] - b[1], c[0] - b[0]) - math.atan2(a[1] - b[1], a[0] - b[0])
                            angle_deg = math.degrees(angle_rad) 
                            return angle_deg


            if round(body_language_prob[np.argmax(body_language_prob)], 2) > 0.75:

                cv2.putText(image, 'EXERCISE',
                            (95, 12), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 1, cv2.LINE_AA)
                cv2.putText(image, body_language_class.split(' ')[0],
                            (90, 40), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)
                cv2.putText(image, 'GRADE',
                            (15, 12), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 1, cv2.LINE_AA)
                
                # Grade lunge exercise performance

                if str(body_language_class) == 'Lunge':
                    if len(results.pose_landmarks.landmark) >= 33:
                        left_hip = (results.pose_landmarks.landmark[mp_pose.PoseLandmark.LEFT_HIP].x,
                                    results.pose_landmarks.landmark[mp_pose.PoseLandmark.LEFT_HIP].y)
                        left_knee = (results.pose_landmarks.landmark[mp_pose.PoseLandmark.LEFT_KNEE].x,
                                    results.pose_landmarks.landmark[mp_pose.PoseLandmark.LEFT_KNEE].y)
                        left_ankle = (results.pose_landmarks.landmark[mp_pose.PoseLandmark.LEFT_ANKLE].x,
                                    results.pose_landmarks.landmark[mp_pose.PoseLandmark.LEFT_ANKLE].y)
                        right_hip = (results.pose_landmarks.landmark[mp_pose.PoseLandmark.RIGHT_HIP].x,
                                    results.pose_landmarks.landmark[mp_pose.PoseLandmark.RIGHT_HIP].y)
                        right_knee = (results.pose_landmarks.landmark[mp_pose.PoseLandmark.RIGHT_KNEE].x,
                                    results.pose_landmarks.landmark[mp_pose.PoseLandmark.RIGHT_KNEE].y)
                        right_ankle = (results.pose_landmarks.landmark[mp_pose.PoseLandmark.RIGHT_ANKLE].x,
                                    results.pose_landmarks.landmark[mp_pose.PoseLandmark.RIGHT_ANKLE].y)

                        def grade_lunge(left_hip, left_knee, left_ankle, right_hip, right_knee, right_ankle):
                            # Calculate the angles at the knee joint
                            left_angle = calculate_angle(left_hip, left_knee, left_ankle)
                            right_angle = calculate_angle(right_hip, right_knee, right_ankle)
                            if left_angle >= 85 and right_angle >= 85:
                                return "Good"
                            else:
                                return "Bad"

                        
                        # Grade lunge exercise performance
                        exercise_grade = grade_lunge(left_hip, left_knee, left_ankle, right_hip, right_knee, right_ankle)
                        landmark_color = grade_colors.get(exercise_grade, (
                            255, 255, 255))  # Default to white if grade is not in the dictionary
                        mp_drawing.draw_landmarks(image, results.pose_landmarks, mp_pose.POSE_CONNECTIONS,
                                                  mp_drawing.DrawingSpec(color=landmark_color, thickness=2,
                                                                         circle_radius=2),
                                                  mp_drawing.DrawingSpec(color=(255, 255, 255), thickness=2,
                                                                         circle_radius=2))
                        # Display exercise grade
                        #cv2.putText(image, 'GRADE',
                                    #(15, 12), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 1, cv2.LINE_AA)
                        cv2.putText(image, exercise_grade,
                                    (10, 40), cv2.FONT_HERSHEY_SIMPLEX, 1, grade_colors.get(exercise_grade, (
                            255, 255, 255)),2, cv2.LINE_AA) #<<<<<<<<<<
                        
                         # Check for literal raise exercise
                if str(body_language_class) == 'Lateral raise':
                    if len(results.pose_landmarks.landmark) >= 13:
                        left_shoulder = (results.pose_landmarks.landmark[mp_pose.PoseLandmark.LEFT_SHOULDER].x,
                                    results.pose_landmarks.landmark[mp_pose.PoseLandmark.LEFT_SHOULDER].y)
                        left_wrist = (results.pose_landmarks.landmark[mp_pose.PoseLandmark.LEFT_WRIST].x,
                                    results.pose_landmarks.landmark[mp_pose.PoseLandmark.LEFT_WRIST].y)
                        right_shoulder = (results.pose_landmarks.landmark[mp_pose.PoseLandmark.RIGHT_SHOULDER].x,
                                    results.pose_landmarks.landmark[mp_pose.PoseLandmark.RIGHT_SHOULDER].y)
                        right_wrist = (results.pose_landmarks.landmark[mp_pose.PoseLandmark.RIGHT_WRIST].x,
                                    results.pose_landmarks.landmark[mp_pose.PoseLandmark.RIGHT_WRIST].y)
                        left_hip = (results.pose_landmarks.landmark[mp_pose.PoseLandmark.LEFT_HIP].x,
                                    results.pose_landmarks.landmark[mp_pose.PoseLandmark.LEFT_HIP].y)
                        right_hip = (results.pose_landmarks.landmark[mp_pose.PoseLandmark.RIGHT_HIP].x,
                                    results.pose_landmarks.landmark[mp_pose.PoseLandmark.RIGHT_HIP].y)

                        def grade_lateral_raise(left_shoulder, left_wrist, right_shoulder, right_wrist,left_hip,right_hip):
                        # Calculate the angle between the shoulders and wrists
                            left_angle = calculate_angle(left_wrist,left_shoulder, left_hip)
                            right_angle = calculate_angle(right_wrist,right_shoulder, right_hip)

                            if (left_angle <= 120 and right_angle <= 120):
                                return "Good"
                            else:
                                return "Bad"
                                    
                        # Check literal raise exercise performance
                        exercise_check = grade_lateral_raise(left_shoulder, left_wrist, right_shoulder, right_wrist,left_hip,right_hip)
                        landmark_color = grade_colors.get(exercise_check, (
                            255, 255, 255))  # Default to white if grade is not in the dictionary
                        mp_drawing.draw_landmarks(image, results.pose_landmarks, mp_pose.POSE_CONNECTIONS,
                                                  mp_drawing.DrawingSpec(color=landmark_color, thickness=2,
                                                                         circle_radius=2),
                                                  mp_drawing.DrawingSpec(color=(255, 255, 255), thickness=2,
                                                                         circle_radius=2))
                        # Display exercise check result                        
                        cv2.putText(image, exercise_check,
                                    (10, 40), cv2.FONT_HERSHEY_SIMPLEX, 1,grade_colors.get(exercise_check, (
                            255, 255, 255)), 2, cv2.LINE_AA)#<<<<<<<<<<<<<<<<<<<<<
                        
                if str(body_language_class) == 'Plank':
                    if len(results.pose_landmarks.landmark) >= 13:
                        left_shoulder = (results.pose_landmarks.landmark[mp_pose.PoseLandmark.LEFT_SHOULDER].x,
                                    results.pose_landmarks.landmark[mp_pose.PoseLandmark.LEFT_SHOULDER].y)
                        right_shoulder = (results.pose_landmarks.landmark[mp_pose.PoseLandmark.RIGHT_SHOULDER].x,
                                    results.pose_landmarks.landmark[mp_pose.PoseLandmark.RIGHT_SHOULDER].y)
                        left_ankle = (results.pose_landmarks.landmark[mp_pose.PoseLandmark.LEFT_ANKLE].x,
                                    results.pose_landmarks.landmark[mp_pose.PoseLandmark.LEFT_ANKLE].y)
                        right_ankle = (results.pose_landmarks.landmark[mp_pose.PoseLandmark.RIGHT_ANKLE].x,
                                    results.pose_landmarks.landmark[mp_pose.PoseLandmark.RIGHT_ANKLE].y)
                        left_hip = (results.pose_landmarks.landmark[mp_pose.PoseLandmark.LEFT_HIP].x,
                                    results.pose_landmarks.landmark[mp_pose.PoseLandmark.LEFT_HIP].y)
                        right_hip = (results.pose_landmarks.landmark[mp_pose.PoseLandmark.RIGHT_HIP].x,
                                    results.pose_landmarks.landmark[mp_pose.PoseLandmark.RIGHT_HIP].y)

                        def grade_plank(left_shoulder, left_ankle, right_shoulder, right_ankle,left_hip,right_hip):
                        # Calculate the angle between the shoulders and wrists
                            left_angle = calculate_angle(left_ankle,left_hip, left_shoulder)
                            right_angle = calculate_angle(right_ankle,right_hip, right_shoulder)

                            if( (left_angle <= 190 and left_angle >= 160) or (right_angle <= 180 and right_angle >= 160)):
                                return "Good"
                            else:
                                return "Bad"
 
                                    
                        # Check literal raise exercise performance
                        exercise_check_plank = grade_plank(left_shoulder, left_ankle, right_shoulder, right_ankle,left_hip,right_hip)

                        landmark_color = grade_colors.get(exercise_check_plank, (
                            255, 255, 255))  # Default to white if grade is not in the dictionary
                        mp_drawing.draw_landmarks(image, results.pose_landmarks, mp_pose.POSE_CONNECTIONS,
                                                  mp_drawing.DrawingSpec(color=landmark_color, thickness=2,
                                                                         circle_radius=2),
                                                  mp_drawing.DrawingSpec(color=(255, 255, 255), thickness=2,
                                                                         circle_radius=2))
                        # Display exercise check result                        
                        cv2.putText(image, exercise_check_plank,
                                    (10, 40), cv2.FONT_HERSHEY_SIMPLEX, 1,grade_colors.get(exercise_check_plank, (
                            255, 255, 255)), 2, cv2.LINE_AA)#<<<<<<<<<<<<<<<<<<

                                     
        except:
            pass

        cv2.imshow('Mediapipe Feed', image)

        if cv2.waitKey(10) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()